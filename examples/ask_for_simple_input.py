import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from lectio.input import userInputPrompt
from lectio.grid import selectGridMethod

#
# Ask for a name:
#
name = userInputPrompt(message="What is your name?", default="Ambrus")

#
# Ask for Integer
#
age = userInputPrompt(
    message="How old are you?", typ="int", extraOptions=False, default=42
)

#
# Ask for float
#
sympathy = userInputPrompt(
    message="How much do you like this library so far?", typ="float", default=1000.0
)


print("Hi, {}! Nice to meet you!".format(name))
print(
    "I am delighted to learn you are {} years old and you like us {} on an arbitrary scale!".format(
        age, sympathy
    )
)

#
# Ask for bool
#
confirmation = userInputPrompt(
    message="Do you want to continue this trial?",
    typ="bool",
    extraOptions=False,
    default=True,
)

if confirmation:
    floatList = userInputPrompt(
        message="Give us a list of floats!", typ="floatList", default=[3.14, 0.1, 66.0]
    )

    intList = userInputPrompt(
        message="Give us a list of integers!",
        typ="intList",
        default=[2, 7, 4, 1, 15, 66],
    )

    strList = userInputPrompt(
        message="Give us a list of strings!",
        typ="strList",
        default=["Dog", "Cat", "Duck", "Bat"],
    )

    dic = userInputPrompt(
        message="Give us a python dictionary!", typ="dict", default={"a": 1, "b": "Dog"}
    )

    method = userInputPrompt(
        message="Define a discretization method for us!",
        typ="method",
        default={"type": "linspace", "min": 0.1, "max": 12.1, "n": 13},
    )

    print(
        "You have defined a list of floats: {}, that is {} in ascending order".format(
            floatList, sorted(floatList)
        )
    )
    print(
        "You have defined a list of integers: {}, that is {} in ascending order".format(
            intList, sorted(intList)
        )
    )
    print(
        "You have defined a list of strigs: {}, that is {} in sorted order".format(
            strList, sorted(strList)
        )
    )
    print("You have defined a dictionary: {}".format(dic))
    array = selectGridMethod(method)
    print(
        "You have defined a discretization method: {} that translates into the following grid: {}, with lenght {}".format(
            method, array, len(array)
        )
    )
