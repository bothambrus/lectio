#################################################################################
#   LectIO                                                                      #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import numpy as np
from .context import lectio
from lectio.input import userInputPrompt
from lectio.grid import selectGridMethod


def test_string_input(mocker):
    mocker.patch("six.moves.input", return_value="abc")
    assert userInputPrompt("a") == "abc"


def test_default_input(mocker):
    #
    # Empty user input meant: take default
    #
    mocker.patch("six.moves.input", return_value="")
    assert userInputPrompt("a", default="def") == "def"

    #
    # If "default" is present in user input, return the string without processing
    #
    mocker.patch("six.moves.input", return_value="defaultMine")
    assert userInputPrompt("a", default="def") == "defaultMine"
    assert userInputPrompt("a", typ="int") == "defaultMine"

    mocker.patch("six.moves.input", return_value='{"default": "extreme case"}')
    assert userInputPrompt("a", default="def") == '{"default": "extreme case"}'


def test_integer_input(mocker):
    mocker.patch("six.moves.input", return_value="13")
    assert (
        userInputPrompt(
            "a",
            typ="int",
            options=["11", "12"],
            optDescr=["eleven", "twevle"],
            default=12,
        )
        == 13
    )

    #
    # Option repetition
    #
    assert isinstance(
        userInputPrompt(
            "a",
            typ="int",
            options=["11", "11", "12"],
            optDescr=["eleven", "eleven", "twevle"],
            default=12,
        ),
        int,
    )

    #
    # Compact options
    #
    assert isinstance(
        userInputPrompt(
            "a", typ="int", options=["11", "12"], compactOptions=True, default=12
        ),
        int,
    )
    assert isinstance(
        userInputPrompt(
            "a",
            typ="int",
            options=[
                "11",
                "12",
                "13",
                "14",
                "15",
                "16",
                "17",
                "18",
                "19",
                "20",
                "21",
                "22",
                "23",
                "24",
                "25",
                "26",
                "27",
                "28",
                "29",
                "30",
                "31",
                "32",
                "333",
                "334",
                "335",
                "3334",
                "3335",
                "3336",
                "33333",
                "33334",
                "33335",
                "33336",
                "33337",
                "33338",
                "33339",
                "444444",
                "5555555",
                "5555556",
                "5555557",
            ],
            compactOptions=True,
            default=12,
        ),
        int,
    )

    #
    # Test non-int
    #
    mocker.patch("six.moves.input", return_value="13.0")
    assert userInputPrompt("a", typ="int") is None


def test_float_input(mocker):
    mocker.patch("six.moves.input", return_value="13.0")
    assert userInputPrompt("a", typ="float", default=12.0) == 13.0
    assert isinstance(userInputPrompt("a", typ="float", default=12.0), float)

    mocker.patch("six.moves.input", return_value="13.5")
    assert userInputPrompt("a", typ="float", default=12.0) == 13.5
    assert isinstance(userInputPrompt("a", typ="float", default=12.0), float)

    #
    # Test non-float
    #
    mocker.patch("six.moves.input", return_value="13.0?")
    assert userInputPrompt("a", typ="float") is None


def test_bool_input(mocker):
    #
    # Affirmative
    #
    mocker.patch("six.moves.input", return_value="True")
    assert userInputPrompt("a", typ="bool")
    assert isinstance(userInputPrompt("a", typ="bool"), bool)
    mocker.patch("six.moves.input", return_value="y")
    assert userInputPrompt("a", typ="bool")
    mocker.patch("six.moves.input", return_value="t")
    assert userInputPrompt("a", typ="bool")

    #
    # Negating
    #
    mocker.patch("six.moves.input", return_value="False")
    assert not userInputPrompt("a", typ="bool")
    assert isinstance(userInputPrompt("a", typ="bool"), bool)
    mocker.patch("six.moves.input", return_value="n")
    assert not userInputPrompt("a", typ="bool")
    mocker.patch("six.moves.input", return_value="f")
    assert not userInputPrompt("a", typ="bool")

    #
    # Other
    #
    mocker.patch("six.moves.input", return_value="what?")
    assert userInputPrompt("a", typ="bool") is None


def test_float_list_input(mocker):
    mocker.patch("six.moves.input", return_value="[13.0,14.0]")
    assert np.all(userInputPrompt("a", typ="floatList") == [13.0, 14.0])
    assert isinstance(userInputPrompt("a", typ="floatList"), list)
    assert isinstance(userInputPrompt("a", typ="floatList")[0], float)

    #
    # Test if brackets are unecessary
    #
    mocker.patch("six.moves.input", return_value="13.0,14.0")
    assert np.all(userInputPrompt("a", typ="floatList") == [13.0, 14.0])

    #
    # Test if decimals are unnecessary
    #
    mocker.patch("six.moves.input", return_value="13,14")
    assert np.all(userInputPrompt("a", typ="floatList") == [13.0, 14.0])

    #
    # Test space separated
    #
    mocker.patch("six.moves.input", return_value="13.0 14.0")
    assert np.all(userInputPrompt("a", typ="floatList") == [13.0, 14.0])


def test_int_list_input(mocker):
    mocker.patch("six.moves.input", return_value="13,14")
    assert np.all(userInputPrompt("a", typ="intList") == [13, 14])
    assert isinstance(userInputPrompt("a", typ="intList"), list)
    assert isinstance(userInputPrompt("a", typ="intList")[0], int)

    #
    # Test if square brackets do not disturb
    #
    mocker.patch("six.moves.input", return_value="[13,14]")
    assert np.all(userInputPrompt("a", typ="intList") == [13, 14])

    #
    # Test spece separated
    #
    mocker.patch("six.moves.input", return_value="13 14")
    assert np.all(userInputPrompt("a", typ="intList") == [13, 14])
    assert isinstance(userInputPrompt("a", typ="intList"), list)
    assert isinstance(userInputPrompt("a", typ="intList")[0], int)

    #
    # Test tab separated
    #
    mocker.patch("six.moves.input", return_value="13\t14")
    assert np.all(userInputPrompt("a", typ="intList") == [13, 14])
    assert isinstance(userInputPrompt("a", typ="intList"), list)
    assert isinstance(userInputPrompt("a", typ="intList")[0], int)


def test_string_list_input(mocker):
    mocker.patch("six.moves.input", return_value="ab,abc,efg")
    assert np.all(userInputPrompt("a", typ="strList") == ["ab", "abc", "efg"])
    assert isinstance(userInputPrompt("a", typ="strList"), list)
    assert isinstance(userInputPrompt("a", typ="strList")[0], str)

    mocker.patch("six.moves.input", return_value="ab abc efg")
    assert np.all(userInputPrompt("a", typ="strList") == ["ab", "abc", "efg"])
    assert isinstance(userInputPrompt("a", typ="strList"), list)
    assert isinstance(userInputPrompt("a", typ="strList")[0], str)


def test_dict_input(mocker):
    mocker.patch("six.moves.input", return_value='"a":12, "b":"Fe"')
    assert userInputPrompt("a", typ="dict")["a"] == 12
    assert userInputPrompt("a", typ="dict")["b"] == "Fe"
    assert isinstance(userInputPrompt("a", typ="dict"), dict)

    #
    # Test different characters
    #
    mocker.patch("six.moves.input", return_value='\'a\':12, "b":u"Fe"')
    assert userInputPrompt("a", typ="dict")["a"] == 12
    assert userInputPrompt("a", typ="dict")["b"] == "Fe"
    assert isinstance(userInputPrompt("a", typ="dict"), dict)


def test_discretization_method_input(mocker):
    #
    # Test without decimal
    #
    mocker.patch("six.moves.input", return_value="1")
    assert userInputPrompt("a", typ="method") == 1.0
    assert selectGridMethod(userInputPrompt("a", typ="method")) == [1.0]
    assert isinstance(userInputPrompt("a", typ="method"), float)

    #
    # Test float with decimal
    #
    mocker.patch("six.moves.input", return_value="12.0")
    assert selectGridMethod(userInputPrompt("a", typ="method")) == [12.0]

    #
    # Test list
    #
    mocker.patch("six.moves.input", return_value="[12,13,14]")
    assert np.all(
        selectGridMethod(userInputPrompt("a", typ="method")) == [12.0, 13.0, 14.0]
    )

    #
    # Test handwritten dictionary
    #
    mocker.patch(
        "six.moves.input", return_value="{'type':'linspace','min':0.6,'max':12,'n':3}"
    )
    assert np.all(
        selectGridMethod(userInputPrompt("a", typ="method")) == [0.6, 6.3, 12.0]
    )

    mocker.patch(
        "six.moves.input",
        return_value="{'type':'geom','min':0.6,'max':12,'n':3, 'r': 1.1}",
    )
    assert (
        np.abs(
            selectGridMethod(userInputPrompt("a", typ="method"))[1] - 6.0285714285714285
        )
        < 1e-6
    )


def test_discretization_method_linspace_input(mocker):
    #
    # Try mocking multiple inputs for linspace
    #
    input_values = ["linspace", "0.1", "10.0", "4"]

    def mock_input(s):
        return input_values.pop(0)

    mocker.patch("six.moves.input", mock_input)
    assert np.all(
        np.abs(
            selectGridMethod(userInputPrompt("a", typ="method"))
            - np.array([0.1, 3.4, 6.7, 10.0])
        )
        < 1e-6
    )

    #
    # Try with type-o
    #
    input_values = ["linsapce", "linspace", "0.1", "10.0", "4"]

    def mock_input(s):
        return input_values.pop(0)

    mocker.patch("six.moves.input", mock_input)
    assert np.all(
        np.abs(
            selectGridMethod(userInputPrompt("a", typ="method"))
            - np.array([0.1, 3.4, 6.7, 10.0])
        )
        < 1e-6
    )

    #
    # Try with defaults
    #
    input_values = ["linspace", "", "", "4"]

    def mock_input(s):
        return input_values.pop(0)

    mocker.patch("six.moves.input", mock_input)
    assert np.all(
        np.abs(
            selectGridMethod(userInputPrompt("a", typ="method"))
            - np.array([0, 0.33333333333, 0.666666666, 1.0])
        )
        < 1e-6
    )

    #
    # Try with supplying defaults
    #
    input_values = ["linspace", "", "", "4"]

    def mock_input(s):
        return input_values.pop(0)

    mocker.patch("six.moves.input", mock_input)
    default = dict(min=0.4, max=0.7, n=3)
    assert np.all(
        np.abs(
            selectGridMethod(userInputPrompt("a", typ="method", default=default))
            - np.array([0.4, 0.5, 0.6, 0.7])
        )
        < 1e-6
    )


def test_discretization_method_power_input(mocker):
    #
    # Try mocking power
    #
    input_values = ["power", "0.1", "10.0", "4", "3.0"]

    def mock_input(s):
        return input_values.pop(0)

    mocker.patch("six.moves.input", mock_input)
    assert np.all(
        np.abs(
            selectGridMethod(userInputPrompt("a", typ="method"))
            - np.array([0.1, 0.466666666, 3.03333333, 10.0])
        )
        < 1e-6
    )

    #
    # Default power
    #
    input_values = ["power", "0.1", "10.0", "4", ""]

    def mock_input(s):
        return input_values.pop(0)

    mocker.patch("six.moves.input", mock_input)
    assert np.all(
        np.abs(
            selectGridMethod(userInputPrompt("a", typ="method"))
            - np.array([0.1, 1.2, 4.5, 10.0])
        )
        < 1e-6
    )

    #
    # Supplying default power
    #
    input_values = ["power", "0.1", "10.0", "4", ""]

    def mock_input(s):
        return input_values.pop(0)

    mocker.patch("six.moves.input", mock_input)
    assert np.all(
        np.abs(
            selectGridMethod(userInputPrompt("a", typ="method", default=dict(pow=4.0)))
            - np.array([0.1, 0.22222222, 2.0555555555, 10.0])
        )
        < 1e-6
    )


def test_discretization_method_geom_input(mocker):
    #
    # Try mocking geom
    #
    input_values = ["geom", "0.1", "10.0", "4", "1.1"]

    def mock_input(s):
        return input_values.pop(0)

    mocker.patch("six.moves.input", mock_input)
    assert np.all(
        np.abs(
            selectGridMethod(userInputPrompt("a", typ="method"))
            - np.array([0.1, 3.09093656, 6.38096677, 10.0])
        )
        < 1e-6
    )

    #
    # Default ratio
    #
    input_values = ["geom", "0.1", "10.0", "4", ""]

    def mock_input(s):
        return input_values.pop(0)

    mocker.patch("six.moves.input", mock_input)
    assert np.all(
        np.abs(
            selectGridMethod(userInputPrompt("a", typ="method"))
            - np.array([0.1, 3.24036479, 6.53774782, 10.0])
        )
        < 1e-6
    )

    #
    # Supplying default ratio
    #
    input_values = ["geom", "0.1", "10.0", "4", ""]

    def mock_input(s):
        return input_values.pop(0)

    mocker.patch("six.moves.input", mock_input)
    assert np.all(
        np.abs(
            selectGridMethod(userInputPrompt("a", typ="method", default=dict(r=1.2)))
            - np.array([0.1, 2.81978022, 6.08351648, 10.0])
        )
        < 1e-6
    )


def test_discretization_method_attractor_input(mocker):
    #
    # Try mocking attractor
    #
    input_values = ["attractor", "0.0", "10.0", "10", "1.1", "3.0"]

    def mock_input(s):
        return input_values.pop(0)

    mocker.patch("six.moves.input", mock_input)
    assert np.all(
        np.abs(
            selectGridMethod(userInputPrompt("a", typ="method"))
            - np.array(
                [
                    0.0,
                    1.09667674,
                    2.09365559,
                    3.0,
                    3.90725166,
                    4.90522849,
                    6.003003,
                    7.21055497,
                    8.53886212,
                    10.0,
                ]
            )
        )
        < 1e-6
    )

    #
    # Default loc
    #
    input_values = ["attractor", "0.0", "10.0", "15", "1.1", ""]

    def mock_input(s):
        return input_values.pop(0)

    mocker.patch("six.moves.input", mock_input)
    assert np.all(
        np.abs(
            selectGridMethod(userInputPrompt("a", typ="method"))
            - np.array(
                [
                    0.0,
                    0.36555891,
                    0.6978852,
                    1.0,
                    1.48566828,
                    2.01990338,
                    2.607562,
                    3.25398648,
                    3.96505341,
                    4.74722702,
                    5.607618,
                    6.55404808,
                    7.59512117,
                    8.74030157,
                    10.0,
                ]
            )
        )
        < 1e-6
    )

    #
    # Supplying default loc
    #
    input_values = ["attractor", "0.0", "10.0", "10", "1.1", ""]

    def mock_input(s):
        return input_values.pop(0)

    mocker.patch("six.moves.input", mock_input)
    assert np.all(
        np.abs(
            selectGridMethod(userInputPrompt("a", typ="method", default=dict(loc=3.0)))
            - np.array(
                [
                    0.0,
                    1.09667674,
                    2.09365559,
                    3.0,
                    3.90725166,
                    4.90522849,
                    6.003003,
                    7.21055497,
                    8.53886212,
                    10.0,
                ]
            )
        )
        < 1e-6
    )


def test_discretization_method_multipleAttractor_input(mocker):
    #
    # Try mocking attractor
    #
    input_values = ["multipleAttractor", "0.0", "10.0", "1.05", "[3.0,7.0]", "[11,11]"]

    def mock_input(s):
        return input_values.pop(0)

    mocker.patch("six.moves.input", mock_input)
    assert np.all(
        np.abs(
            selectGridMethod(userInputPrompt("a", typ="method"))
            - np.array(
                [
                    0.00000000e00,
                    5.62907052e-01,
                    1.09900901e00,
                    1.60958230e00,
                    2.09584257e00,
                    2.55894760e00,
                    3.00000000e00,
                    3.46402367e00,
                    3.95124851e00,
                    4.46283460e00,
                    5.00000000e00,
                    5.43995200e00,
                    5.85895390e00,
                    6.25800333e00,
                    6.63805040e00,
                    7.00000000e00,
                    7.54292439e00,
                    8.11299501e00,
                    8.71156915e00,
                    9.34007201e00,
                    1.00000000e01,
                ]
            )
        )
        < 1e-6
    )

    #
    # Test passing defaults
    #
    input_values = ["multipleAttractor", "0.0", "10.0", "", "", ""]

    def mock_input(s):
        return input_values.pop(0)

    mocker.patch("six.moves.input", mock_input)
    assert np.all(
        np.abs(
            selectGridMethod(
                userInputPrompt(
                    "a",
                    typ="method",
                    default=dict(locList=[3, 7], sectionLengths=[11, 11]),
                )
            )
            - np.array(
                [
                    0.00000000e00,
                    5.62907052e-01,
                    1.09900901e00,
                    1.60958230e00,
                    2.09584257e00,
                    2.55894760e00,
                    3.00000000e00,
                    3.46402367e00,
                    3.95124851e00,
                    4.46283460e00,
                    5.00000000e00,
                    5.43995200e00,
                    5.85895390e00,
                    6.25800333e00,
                    6.63805040e00,
                    7.00000000e00,
                    7.54292439e00,
                    8.11299501e00,
                    8.71156915e00,
                    9.34007201e00,
                    1.00000000e01,
                ]
            )
        )
        < 1e-6
    )
