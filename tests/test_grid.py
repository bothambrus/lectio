#################################################################################
#   LectIO                                                                      #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import numpy as np
from .context import lectio
from lectio.grid import selectGridMethod


#
# Testing grid methods
#
def test_int_grid():
    """
    Test if misleading integer based grid is removed.
    """
    assert selectGridMethod(3) is None


def test_float_grid():
    """
    Test float based grid.
    """
    assert all(selectGridMethod(0.5) == np.array([0.5]))


def test_list_grid():
    """
    Test list based grid.
    """
    assert all(selectGridMethod([0.1, 0.2]) == np.array([0.1, 0.2]))


def test_linspace_grid():
    """
    Test 'linspace' grid.
    """
    method = {
        "type": "linspace",
        "min": 0.2,
        "max": 0.8,
        "n": 5,
        "expectedResult": np.array([0.2, 0.35, 0.5, 0.65, 0.8]),
    }
    assert all(np.abs(selectGridMethod(method) - method["expectedResult"]) < 1e-10)

    method2 = {
        "type": "linspace",
        "min": 0.2,
        "max": 0.8,
        "n": 1,
        "expectedResult": np.array([0.2]),
    }
    assert all(np.abs(selectGridMethod(method2) - method2["expectedResult"]) < 1e-10)

    method3 = {
        "type": "linspace",
        "min": 0.2,
        "max": 0.8,
        "n": 2,
        "expectedResult": np.array([0.2, 0.8]),
    }
    assert all(np.abs(selectGridMethod(method3) - method3["expectedResult"]) < 1e-10)


def test_geom_grid():
    """
    Test 'geom' grid.
    """
    method = {
        "type": "geom",
        "min": 0.2,
        "max": 0.8,
        "n": 5,
        "r": 1.1,
        "expectedResult": np.array([0.2, 0.32928248, 0.47149321, 0.62792502, 0.8]),
    }
    assert all(np.abs(selectGridMethod(method) - method["expectedResult"]) < 1e-8)

    method2 = {
        "type": "geom",
        "min": 0.2,
        "max": 0.8,
        "n": 5,
        "r": 1,
        "expectedResult": np.array([0.2, 0.35, 0.5, 0.65, 0.8]),
    }
    assert all(np.abs(selectGridMethod(method2) - method2["expectedResult"]) < 1e-10)


def test_power_grid():
    """
    Test 'power' grid.
    """
    method = {
        "type": "power",
        "min": 0.2,
        "max": 0.8,
        "n": 5,
        "pow": 2,
        "expectedResult": np.array([0.2, 0.2375, 0.35, 0.5375, 0.8]),
    }
    assert all(np.abs(selectGridMethod(method) - method["expectedResult"]) < 1e-10)


def test_attractor_grid():
    """
    Test 'attractor' grid.
    """
    method = {
        "type": "attractor",
        "min": 0.2,
        "max": 0.8,
        "n": 10,
        "r": 1.1,
        "loc": 0.4,
        "expectedResult": np.array(
            [
                0.2,
                0.25735833,
                0.30950226,
                0.35690584,
                0.4,
                0.46551899,
                0.53758988,
                0.61686786,
                0.70407364,
                0.8,
            ]
        ),
    }
    assert all(np.abs(selectGridMethod(method) - method["expectedResult"]) < 1e-8)


def test_multipleAttractor_grid():
    """
    Test 'multipleAttractor' grid.
    """
    method = {
        "type": "multipleAttractor",
        "min": 0.2,
        "max": 0.8,
        "sectionLengths": [10, 10, 10],
        "r": 1.1,
        "locList": [0.4, 0.5, 0.6],
        "expectedResult": np.array(
            [
                2.00000000e-01,
                2.37346454e-01,
                2.71297777e-01,
                3.02162615e-01,
                3.30221559e-01,
                3.55729690e-01,
                3.78918900e-01,
                4.00000000e-01,
                4.23809524e-01,
                4.50000000e-01,
                4.61990795e-01,
                4.72891517e-01,
                4.82801265e-01,
                4.91810126e-01,
                5.00000000e-01,
                5.10773540e-01,
                5.22624434e-01,
                5.35660418e-01,
                5.50000000e-01,
                5.68277946e-01,
                5.84894260e-01,
                6.00000000e-01,
                6.25921476e-01,
                6.54435100e-01,
                6.85800086e-01,
                7.20301570e-01,
                7.58253204e-01,
                8.00000000e-01,
            ]
        ),
    }
    print(selectGridMethod(method))
    assert all(np.abs(selectGridMethod(method) - method["expectedResult"]) < 1e-8)
