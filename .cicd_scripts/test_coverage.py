import re
import sys
import xml.etree.ElementTree as ET


def main():
    coverage_threshold = float(sys.argv[1])
    xml_file = sys.argv[2]

    tree = ET.parse(xml_file)
    root = tree.getroot()

    current_coverage = float(root.attrib["line-rate"]) * 100.0

    print('Current coverage: {}'.format(current_coverage))

    if current_coverage < coverage_threshold:
        sys.exit(
            "FAILED: coverage threshold {0} (current) < {1} (threshold)".format(
                current_coverage, coverage_threshold
            )
        )


if __name__ == "__main__":
    main()
