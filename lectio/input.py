#################################################################################
#   LectIO                                                                      #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import json

import six.moves


def userInputPrompt(
    message,
    typ="str",
    default=None,
    options=None,
    optDescr=None,
    compactOptions=False,
    extraOptions=True,
):
    """
    Ask the user in a command line prompt to fill in a value.
    `message`:         message displayed as the prompt
    `typ`:             type of input:
                       `str`:         normal string input
                       `float`:       floating point number
                       `int`:         integer number
                       `bool`:        logical value
                       `floatList`:   list of floating point numbers (comma separated)
                       `intList`:     list of integer numbers (comma separated)
                       `strList`:     list of strings (comma separated)
                       `dict`:        python dictionary
                       `method`:      a method describing a floating point sequence
    `default`:         default value
    `options`:         options to offer to the user
    `optDescr`:        description of each offered option
    `compactOptions`:  if options should be listed in one line without description
    `extraOptions`:    if additional options should be displayed as help
    """
    if options is None:
        options = []
    elif len(options) > 1:
        #
        # Clean up duplicates
        #
        iop = 1
        while iop < len(options):
            if options[iop] == options[0]:
                del options[iop]
            else:
                iop += 1
    if optDescr is None:
        optDescr = []
    #
    # Append default suggestions for specific types
    #
    if extraOptions:
        if typ == "floatList":
            options.append("val1,val2,val3,...")
            optDescr.append("list of real values")
            options.append("[]")
            optDescr.append("empty list")
        elif typ == "int":
            options.append("n")
            optDescr.append("integer")
        elif typ == "intList":
            options.append("n1,n2,n3,...")
            optDescr.append("list of integers")
            options.append("[]")
            optDescr.append("empty list")
        elif typ == "strList":
            options.append("str1,str2,str3,...")
            optDescr.append("list of words")
            options.append("[]")
            optDescr.append("empty list")
        elif typ == "dict":
            options.append("'key1':val1,'key2':val2,...")
            optDescr.append("dictionary with all entries")
        elif typ == "bool":
            options.append("y/n/t/f/True/False")
            optDescr.append("logical input")
        elif typ == "method":
            options.append("val")
            optDescr.append("single value in list")
            options.append("[val1,val2,...]")
            optDescr.append("list of values")
            options.append("{'key1':val1,...}")
            optDescr.append("dictionary of discretization method")
            options.append("linspace")
            optDescr.append("define number of values between given limits")
            options.append("power")
            optDescr.append(
                "power function scaled between limiting values: min + (max-min) * (0.0 ... 1.0)^p"
            )
            options.append("geom")
            optDescr.append(
                "geometric sequence scaled between limiting values: min + (max-min) * r^(0 ... n)"
            )
            options.append("attractor")
            optDescr.append("two geometric sequences focusing on given location")
            options.append("multipleAttractor")
            optDescr.append("geometric sequences focusing on multiple locations")

    #
    # Give current or default value
    #
    defString = ""
    if not (default is None):
        defString = "\n  Current value: {}".format(default)

    #
    # Give suggestions
    #
    optString = ""
    if len(options) > 0:
        optString = "\n  Suggestions:\n"
        maxleng = 0
        #
        # Get alignment
        #
        for iop, op in enumerate(options):
            maxleng = max(maxleng, len(op))
        if compactOptions:
            nopline = 120 // (maxleng + 1)
            if nopline == 0:
                nopline = 1
            form = "{{:<{}}}".format(maxleng + 1)
            doneops = False
            iop = 0
            while not doneops:
                optString += "    "
                for k in range(nopline):
                    if iop < len(options):
                        optString += form.format(options[iop])
                    iop += 1
                optString += "\n"

                if iop >= len(options):
                    doneops = True

        else:
            form = "    {{:<{}}}{{}}\n".format(10 * (maxleng // 10 + 1))
            for iop, op in enumerate(options):
                od = ""
                if len(optDescr) > iop:
                    od = optDescr[iop]
                optString += form.format(op + ":", od)

    #
    # Get string types:
    #
    strtypes = ()
    try:
        isinstance("a", str)
        strtypes += (str,)
    except:
        pass
    try:
        isinstance("a", unicode)
        strtypes += (str,)
    except:
        pass

    #
    # Ask for input
    #
    val = six.moves.input("{}{}{}> ".format(message, defString, optString))
    if val == "":
        val = default
    #
    # If it has default in it, return
    #
    if isinstance(val, strtypes):
        if "default" in val.lower():
            return val

    if typ == "float":
        #
        # Ask for numerical input
        #
        try:
            val = eval(str(val))
        except:
            val = None
    elif typ == "int":
        #
        # Ask for numerical input
        #
        try:
            val = eval(str(int(val)))
        except:
            val = None
    elif typ == "floatList":
        #
        # Ask for list of numerical inputs
        #
        if isinstance(val, str):
            if "," in val:
                li = val.strip("[").strip("]").split(",")
            else:
                li = val.strip("[").strip("]").split()
            val = []
            for l in li:
                val.append(float(l))
    elif typ == "intList":
        #
        # Ask for list of integers
        #
        if isinstance(val, str):
            if "," in val:
                li = val.strip("[").strip("]").split(",")
            else:
                li = val.strip("[").strip("]").split()
            val = []
            for l in li:
                val.append(int(l))
    elif typ == "strList":
        #
        # Ask for list of integers
        #
        if isinstance(val, str):
            if "," in val:
                li = val.strip("[").strip("]").split(",")
            else:
                li = val.strip("[").strip("]").split()
            val = []
            for l in li:
                val.append(l.strip().strip("'").strip('"'))
    elif typ == "bool":
        #
        # Ask for logical input
        #
        val = str(val)
        if val in ["y", "Y", "t", "T", "True", "true"]:
            val = True
        elif val in ["n", "N", "f", "F", "False", "false"]:
            val = False
        else:
            val = None
    elif typ == "dict":
        #
        # Ask for dictionary entries
        #
        val = str(val)
        if not ("{" in val):
            val = "{" + val
        if not ("}" in val):
            val = val + "}"
        val = val.replace("'", '"')
        val = val.replace('u"', '"')
        val = json.loads(val)
    elif typ == "method":
        #
        # Ask for method of discretization in one dimension
        #
        val = str(val)

        #
        # Single float value
        #
        try:
            val = float(val)
        except:
            #
            # Check if interpretable as list or dictionary
            #
            val = val.strip()
            if (val[0] == "[" and val[-1] == "]") or (val[0] == "{" and val[-1] == "}"):
                val = eval(val)
            else:
                #
                # Process string input
                #
                method = val
                val = methodUserInputPrompt(method, defaultKeys=default)

    return val


def methodUserInputPrompt(method, defaultKeys={}):
    """
    Ask the user in a command line prompt to define a method to discretize a range.
    """
    val = {}

    valid = False
    validOptions = ["linspace", "power", "geom", "attractor", "multipleAttractor"]
    valid = method in validOptions
    if not (valid):
        while not (valid):
            key = "type"
            method = userInputPrompt(
                "Select a valid option",
                typ="str",
                default=validOptions[0],
                options=validOptions,
            )
            valid = method in validOptions

    val["type"] = method

    try:
        defaultKeys = eval(str(dict(defaultKeys)))
    except:
        defaultKeys = {}

    #
    # Read min max and numbers
    #
    key = "min"
    if key in defaultKeys.keys():
        default = defaultKeys[key]
    else:
        default = 0.0
    val[key] = userInputPrompt("lower limit of range", typ="float", default=default)

    key = "max"
    if key in defaultKeys.keys():
        default = defaultKeys[key]
    else:
        default = 1.0
    val[key] = userInputPrompt("upper limit of range", typ="float", default=default)

    #
    # Read number of points
    #
    if method != "multipleAttractor":
        key = "n"
        if key in defaultKeys.keys():
            default = defaultKeys[key]
        else:
            default = 101
        val[key] = userInputPrompt("number of points", typ="int", default=default)

    #
    # Power of power method
    #
    if method == "power":
        key = "pow"
        if key in defaultKeys.keys():
            default = defaultKeys[key]
        else:
            default = 2.0
        val[key] = userInputPrompt("power", typ="float", default=default)

    #
    # Ratio of geom attractor and multipleAttractor
    #
    if method == "geom" or method == "attractor" or method == "multipleAttractor":
        key = "r"
        if key in defaultKeys.keys():
            default = defaultKeys[key]
        else:
            default = 1.05
        val[key] = userInputPrompt("r", typ="float", default=default)

    #
    # Location of attractor:
    #
    if method == "attractor":
        key = "loc"
        if key in defaultKeys.keys():
            default = defaultKeys[key]
        else:
            default = 1.0
        val[key] = userInputPrompt("Location", typ="float", default=default)

    #
    # Location list
    #
    if method == "multipleAttractor":
        key = "locList"
        if key in defaultKeys.keys():
            default = defaultKeys[key]
        else:
            default = [1.0]
        val[key] = userInputPrompt("Location", typ="floatList", default=default)

        key = "sectionLengths"
        if key in defaultKeys.keys():
            default = defaultKeys[key]
        else:
            default = [50]
        val[key] = userInputPrompt("Section lengths", typ="intList", default=default)

    return val
