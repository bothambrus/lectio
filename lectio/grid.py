#################################################################################
#   LectIO                                                                      #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import numpy as np


def selectGridMethod(method):
    """Create numpy array based on different strategies.

    Parameters
    ----------
    method
        The input of this function. It can take various input types.

    Returns
    -------
    np.ndarray
        The resulting numpy array.

    Notes
    -----
    If the `method` is a list, it convets it to a numpy array.

    If the `method` is a dictionary, it is processed as follows:

    -  if `method["type"]` is `"linspace"`:

    .. math::
        min + (max - min) * [0..1]

    -  if `method["type"]` is `"power"`:

    .. math::
        min + (max - min) * [0..1]^{pow}

    -  if `method["type"]` is `"geom"`: Generates `method["n"]` elements with a geometrically growing step size with growth rate `method["r"]`.
    -  if `method["type"]` is `"attractor"`: Generates `method["n"]` elements concentrated around `method["loc"]` with a geometrically growing step size towards `method["max"]` and `method["min"]` with growth rate `method["r"]`. The number of elements on the two sides of `method["loc"]` is adjusted such, that teh step size is smooth at `method["loc"]`.
    -  if `method["type"]` is `"multipleAttractor"`: Based on a list of locations: `method["locList"]` and a list of manually defined section lengths: `method["sectionLengths"]` it uses the attractor function multiple times.
    """
    if isinstance(method, float):
        return np.array([method])

    elif isinstance(method, list):
        if isinstance(method[0], float) or isinstance(method[0], int):
            ret = [float(m) for m in method]
            return np.array(ret)

    elif isinstance(method, dict):
        if method["type"] == "linspace":
            if method["n"] == 1:
                return np.array([method["min"]])
            elif method["n"] == 2:
                return np.array([method["min"], method["max"]])
            else:
                return np.linspace(method["min"], method["max"], method["n"])

        elif method["type"] == "power":
            return method["min"] + (method["max"] - method["min"]) * np.power(
                np.linspace(0.0, 1.0, method["n"]), method["pow"]
            )

        elif method["type"] == "geom":
            r = method["r"]
            n = method["n"]
            if r != 1.0:
                dLeft = (r - 1) / (r ** (n - 1) - 1)
            else:
                dLeft = 1.0 / (n - 1)

            returnArray = [0]
            step = dLeft
            for i in range(n - 1):
                returnArray.append(returnArray[-1] + step)
                step *= r

            return method["min"] + (method["max"] - method["min"]) * np.array(
                returnArray
            )

        elif method["type"] == "attractor":
            r = method["r"]
            n = method["n"]
            nLow = 1
            done = False

            for i in range(n):
                partLow = selectGridMethod(
                    method={
                        "type": "geom",
                        "n": nLow + 1,
                        "min": method["min"],
                        "max": method["loc"],
                        "r": 1 / r,
                    }
                )
                partHigh = selectGridMethod(
                    method={
                        "type": "geom",
                        "n": n - nLow,
                        "min": method["loc"],
                        "max": method["max"],
                        "r": r,
                    }
                )
                stepLow = partLow[-1] - partLow[-2]
                stepHigh = partHigh[1] - partHigh[0]
                if stepLow > stepHigh:
                    nLow += 1
                else:
                    nLow -= 1
                    done = True

                nLow = max(nLow, 1)
                if done:
                    break

            return np.append(partLow[0:-1], partHigh)

        elif method["type"] == "multipleAttractor":
            locList = method["locList"]
            sectionLengths = method["sectionLengths"]
            nSections = len(locList)

            minList = []
            maxList = []

            for i in range(nSections):
                if i == 0:
                    minList.append(method["min"])
                else:
                    minList.append((locList[i - 1] + locList[i]) / 2)

                if i == nSections - 1:
                    maxList.append(method["max"])
                else:
                    maxList.append((locList[i] + locList[i + 1]) / 2)

            parts = np.array([])
            for i in range(nSections):
                p = selectGridMethod(
                    method={
                        "type": "attractor",
                        "n": sectionLengths[i],
                        "min": minList[i],
                        "max": maxList[i],
                        "r": method["r"],
                        "loc": locList[i],
                    }
                )
                parts = np.append(parts, p)

            parts = np.unique(parts)

            return parts
