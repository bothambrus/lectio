# LectIO

I/O tool asking for user input in the command line

For details on usage, see the [wiki](https://gitlab.com/bothambrus/lectio/-/wikis/home).
