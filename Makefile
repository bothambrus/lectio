init:
	pip3 install -r requirements.txt

checkstyle:
	black --check lectio 
	black --check tests
	black --check examples
	isort --profile black --diff lectio 
	isort --profile black --diff tests
	isort --profile black --diff examples

style:
	isort --profile black lectio
	black lectio 
	black tests
	black examples

test:
	rm -rf tests/tmp_testing/
	pytest --junitxml=junit_report.xml --cov=lectio --cov-report=html tests
	coverage xml
	mkdir -p dist 
	rm -rf dist/htmlcov
	mv coverage.xml dist/
	mv htmlcov dist/
	echo open dist/htmlcov/index.html to review code coverage
	python .cicd_scripts/test_coverage.py 90 dist/coverage.xml



.PHONY: init test

